$(document).ready(function() {
	$('.user_dropdown').dropdown();
	$('.datepicker').datepicker();
	$('.timepicker').timepicker();
	$('.collapsible').collapsible();
	$('select').formSelect(); 
    $('.modal').modal({
    	opacity: 0.7
    }); 
}); 